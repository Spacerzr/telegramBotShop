package ru.dgzorin.gulasal_delivery_food.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/*
 * Класс для настройки базы данных 
 */
public class Database {

	private static Connection connection;
	private static PreparedStatement psStartingText;
	private static PreparedStatement psGetFoodList;
	private static PreparedStatement psAddNewClient;
	private static PreparedStatement psGetAddresses;
	private static PreparedStatement pseditFoodTable;
	private static PreparedStatement psaddNewToFoodTable;
	private static PreparedStatement psremoveFromFoodTable;

	private static PreparedStatement psEditStartingText;

	private static String dbName = "deliveryfood";

	public synchronized static void connect() throws Exception {

		Class.forName("org.sqlite.JDBC");
		connection = DriverManager.getConnection("JDBC:sqlite:" + dbName + ".db");

		psStartingText = connection.prepareStatement("SELECT * FROM starting_text;");
	}

	public static ResultSet getFoodList(String tableName) {

		try {
			psGetFoodList = connection
					.prepareStatement("SELECT name, price, photoname, description, id FROM " + tableName + ";");
			ResultSet rs = psGetFoodList.executeQuery();

			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static ResultSet getOneStringFromFoodList(String tableName, String name) {

		try {
			psGetFoodList = connection.prepareStatement(
					"SELECT price, photoname, description, id FROM " + tableName + " WHERE name = '" + name + "';");
			ResultSet rs = psGetFoodList.executeQuery();

			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static HashMap<String, Integer> getAllMenu() {
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		ResultSet temp;

		try {
			temp = getFoodList("soups");
			while (temp.next()) {
				map.put(temp.getString(1), temp.getInt(2));
			}

			temp = getFoodList("salats");
			while (temp.next()) {
				map.put(temp.getString(1), temp.getInt(2));
			}

			temp = getFoodList("pastry");
			while (temp.next()) {
				map.put(temp.getString(1), temp.getInt(2));
			}

			temp = getFoodList("hot");
			while (temp.next()) {
				map.put(temp.getString(1), temp.getInt(2));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;

	}

	public static HashMap<String, String> getStartingText() {
		HashMap<String, String> map = new HashMap<String, String>();

		try {
			ResultSet rs = psStartingText.executeQuery();
			while (rs.next()) {
				map.put(rs.getString(1), rs.getString(2));
			}
			return map;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static boolean getClinetChatId(String chat_id) {
		try {
			psGetFoodList = connection.prepareStatement("SELECT chat_id FROM clients;");

			ResultSet rs = psGetFoodList.executeQuery();

			while (rs.next()) {
				if (rs.getString(1).equals(chat_id)) {
					return true;
				}
			}
			return false;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static ArrayList<String> getChatIdList() {
		try {
			psGetFoodList = connection.prepareStatement("SELECT chat_id FROM clients;");

			ArrayList<String> list = new ArrayList<>();
			ResultSet rs = psGetFoodList.executeQuery();

			while (rs.next()) {
				list.add(rs.getString(1));
			}
			return list;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String getAddress(String chat_id) {
		try {
			psGetFoodList = connection
					.prepareStatement("SELECT address FROM clients WHERE chat_id = '" + chat_id + "';");
			ResultSet rs = psGetFoodList.executeQuery();
			String address = rs.getString(1);
			return address;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String getPhoneNumber(String chat_id) {
		try {
			psGetFoodList = connection
					.prepareStatement("SELECT phoneNumber FROM clients WHERE chat_id = '" + chat_id + "';");
			ResultSet rs = psGetFoodList.executeQuery();
			String address = rs.getString(1);
			return address;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void addClientAddress(String chat_id, String address) {
		try {
			psAddNewClient = connection.prepareStatement(
					"UPDATE clients SET address = '" + address + "' WHERE chat_id = '" + chat_id + "';");
			psAddNewClient.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void addClientPhoneNumber(String chat_id, String phoneNumber) {
		try {
			psAddNewClient = connection.prepareStatement(
					"UPDATE clients SET phoneNumber = '" + phoneNumber + "' WHERE chat_id = '" + chat_id + "';");
			psAddNewClient.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void addNewClient(String chat_id, String name) {
		try {
			psAddNewClient = connection
					.prepareStatement("INSERT INTO clients (chat_id, address, phoneNumber, name) VALUES (\"" + chat_id
							+ "\", \"\",\"\", '" + name + "');");
			psAddNewClient.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void editStartingText(String command, String newValue) {
		try {
			psAddNewClient = connection.prepareStatement(
					"UPDATE starting_text SET text = '" + newValue + "' WHERE command = '" + command + "';");
			psAddNewClient.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void editFoodTable(String tableName, String name, Integer price, String photoName, String description,
			Integer id) {
		try {
			pseditFoodTable = connection.prepareStatement(
					"UPDATE " + tableName + " SET name = '" + name + "', price = " + price + ", photoname = '"
							+ photoName + "', description = '" + description + "' WHERE id = " + id + ";");
			pseditFoodTable.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void addNewToFoodTable(String tableName, String name, Integer price, String photoName,
			String description, Integer id) {
		try {
			psaddNewToFoodTable = connection.prepareStatement(
					"INSERT INTO " + tableName + " (id, name, price, photoname, description) VALUES (" + id + ", \""
							+ name + "\", " + price + ", \"" + photoName + "\", \"" + description + "\");");
			psaddNewToFoodTable.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void removeFromFoodTable(String tableName, String name) {
		try {
			psremoveFromFoodTable = connection
					.prepareStatement("DELETE FROM " + tableName + " WHERE name = \"" + name + "\";");
			psremoveFromFoodTable.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public synchronized static void disconnect() {
		try {
			connection.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
