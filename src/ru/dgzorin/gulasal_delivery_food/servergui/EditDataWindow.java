package ru.dgzorin.gulasal_delivery_food.servergui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import ru.dgzorin.gulasal_delivery_food.database.Database;

public class EditDataWindow extends JFrame {

	private static final int WINDOW_WIDTH = 300;
	private static final int WINDOW_HEIGHT = 170;
	private final String onlyNumbers = "0123456789";
	private final String sucsessChangeData = "Данные в таблице успешно изменены !";
	private final String errorChangeData = "Необходимо заполнить все поля !";

	private final JPanel mainPanel = new JPanel(new BorderLayout());
	private final JPanel secondPanel = new JPanel(new FlowLayout());
	private final JLabel label = new JLabel("Выберите таблицу для редактирования данных:");
	private final Color lightBlue = new Color(120, 175, 255);

	private final JButton btnStartText = new JButton("Текст сообщений");
	private final JButton btnTableHot = new JButton("Таблица \"Горячие блюда\"");
	private final JButton btnTablePastry = new JButton("Таблица \"Выпечка\"");
	private final JButton btnTableSalats = new JButton("Таблица \"Салаты\"");
	private final JButton btnTableSoups = new JButton("Таблица \"Супы\"");

	private JFrame editTableStartingText;
	private JFrame editFoodData;
	private JFrame windowApplyChange;

	EditDataWindow() {
		initGUI();
	}

	private void initGUI() {

		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setLocationRelativeTo(null);
		setTitle("Редактирование");
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		secondPanel.setBackground(Color.GRAY);
		label.setOpaque(true);
		label.setBackground(lightBlue);
		btnStartText.setBackground(lightBlue);
		btnTableHot.setBackground(lightBlue);
		btnTablePastry.setBackground(lightBlue);
		btnTableSalats.setBackground(lightBlue);
		btnTableSoups.setBackground(lightBlue);

		secondPanel.add(btnStartText);
		secondPanel.add(btnTableHot);
		secondPanel.add(btnTablePastry);
		secondPanel.add(btnTableSalats);
		secondPanel.add(btnTableSoups);

		mainPanel.add(label, BorderLayout.NORTH);
		mainPanel.add(secondPanel, BorderLayout.CENTER);
		add(mainPanel);

		btnStartText.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				editTableStartingText();
			}
		});

		btnTableHot.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				editFoodData("hot");
			}
		});

		btnTablePastry.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				editFoodData("pastry");
			}
		});

		btnTableSalats.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				editFoodData("salats");
			}
		});

		btnTableSoups.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				editFoodData("soups");
			}
		});

		addWindowListener(new WindowListener() {

			@Override
			public void windowClosing(WindowEvent e) {
				Database.disconnect();
				ServerGUI.sendMsgToLog("Редактирование данных завершено.");
				ServerGUI.sendMsgToLog("Disconnected from data base.");
			}

			@Override
			public void windowOpened(WindowEvent e) {
				try {
					Database.connect();
					ServerGUI.sendMsgToLog("Connect to database was successful");
				} catch (Exception ex) {
					ServerGUI.sendMsgToLog("Connect to database : ERROR");
				}
			}

			@Override
			public void windowIconified(WindowEvent e) {
			}

			@Override
			public void windowDeiconified(WindowEvent e) {
			}

			@Override
			public void windowDeactivated(WindowEvent e) {
			}

			@Override
			public void windowClosed(WindowEvent e) {
			}

			@Override
			public void windowActivated(WindowEvent e) {
			}
		});
	}

	private void editTableStartingText() {
		editTableStartingText = new JFrame("Таблица сообщений бота");
		JPanel mainPanel = new JPanel(new GridLayout(2, 1));
		JPanel upPanel = new JPanel(new FlowLayout());
		JPanel centerPanel = new JPanel(new FlowLayout());
		JPanel downPanel = new JPanel(new FlowLayout());

		JLabel label = new JLabel("Выберите команду из списка: ");
		JLabel label1 = new JLabel("Измените данные и нажмите \"Изменить\"");
		JButton btnApplyChange = new JButton("Изменить");

		JTextArea changeData = new JTextArea(5, 23);
		JScrollPane scrollPane = new JScrollPane(changeData, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		int width = 400;
		int height = 200;
		HashMap<String, String> map = Database.getStartingText();
		JComboBox<String> comboBox = new JComboBox<String>();

		for (HashMap.Entry<String, String> pair : map.entrySet()) {
			comboBox.addItem(pair.getKey());
		}
		// comboBox.setSelectedIndex(0);
		comboBox.setSelectedItem(null);
		// changeData.setText(map.get(comboBox.getSelectedItem()));

		editTableStartingText.setSize(width, height);
		editTableStartingText.setResizable(false);
		editTableStartingText.setLocationRelativeTo(null);
		editTableStartingText.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		changeData.setLineWrap(true);
		changeData.setWrapStyleWord(true);
		label.setOpaque(true);
		label.setBackground(lightBlue);
		label1.setOpaque(true);
		label1.setBackground(lightBlue);

		upPanel.setBackground(Color.GRAY);
		centerPanel.setBackground(Color.GRAY);
		downPanel.setBackground(Color.GRAY);

		upPanel.add(label);
		upPanel.add(comboBox);
		centerPanel.add(label1);
		upPanel.add(centerPanel);
		downPanel.add(scrollPane);
		downPanel.add(btnApplyChange);

		mainPanel.add(upPanel);
		mainPanel.add(downPanel);

		editTableStartingText.add(mainPanel);
		editTableStartingText.setVisible(true);

		comboBox.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				changeData.setText(map.get(comboBox.getSelectedItem()));
			}
		});

		btnApplyChange.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (comboBox.getSelectedItem() != null) {
					Database.editStartingText(comboBox.getSelectedItem().toString(), changeData.getText());
					windowApplyChange("starting_text", sucsessChangeData);
					sendChangeToLog("Изменено стартовое сообщение для команды " + comboBox.getSelectedItem());
					editTableStartingText.dispose();
				}
			}
		});
	}

	private void editFoodData(String tableName) {

		editFoodData = new JFrame("Таблица " + tableName);
		int width = 400;
		int height = 340;
		editFoodData.setSize(width, height);
		editFoodData.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		editFoodData.setLocationRelativeTo(null);
		editFoodData.setResizable(false);

		JPanel mainMainPanel = new JPanel(new BorderLayout());

		JPanel mainPanel = new JPanel(new GridLayout(2, 1));
		JPanel upPanel = new JPanel(new GridLayout(4, 1));
		JPanel downPanel = new JPanel(new GridLayout(2, 1));

		JPanel panel1 = new JPanel(new FlowLayout());
		JPanel panel2 = new JPanel(new FlowLayout());
		JPanel panel3 = new JPanel(new FlowLayout());
		JPanel panel4 = new JPanel(new FlowLayout());
		JPanel panel5 = new JPanel(new FlowLayout());
		JPanel panel6 = new JPanel(new FlowLayout());

		JComboBox<String> comboBox = new JComboBox<String>();
		JLabel label1 = new JLabel(" Выберите блюдо из списка : ");

		JLabel label2 = new JLabel("      Название : ");
		JLabel label3 = new JLabel("              Цена : ");
		JLabel label4 = new JLabel(" Путь к фото : ");
		JLabel label5 = new JLabel("    Описание : ");

		JTextField tfName = new JTextField(20);
		JTextField tfPrice = new JTextField(20);
		JTextField tfPhoto = new JTextField(20);
		JTextArea taDescription = new JTextArea(4, 19);
		JScrollPane scrollPane = new JScrollPane(taDescription, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		JButton btnEdit = new JButton("Изменить данные блюда");
		JButton btnCreate = new JButton("Добавить");
		JButton btnRemove = new JButton("Удалить");

		btnEdit.setEnabled(false);
		btnCreate.setEnabled(false);
		btnRemove.setEnabled(false);

		ResultSet rs = Database.getFoodList(tableName);

		HashMap<String, ResultSet> map = new HashMap<>();
		try {
			comboBox.addItem("добавить новое блюдо");
			while (rs.next()) {
				comboBox.addItem(rs.getString(1));
				map.put(rs.getString(1), Database.getOneStringFromFoodList(tableName, rs.getString(1)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		comboBox.setSelectedItem(null);
		taDescription.setLineWrap(true);
		taDescription.setWrapStyleWord(true);

		label1.setOpaque(true);
		label1.setBackground(lightBlue);
		label2.setOpaque(true);
		label2.setBackground(lightBlue);
		label3.setOpaque(true);
		label3.setBackground(lightBlue);
		label4.setOpaque(true);
		label4.setBackground(lightBlue);
		label5.setOpaque(true);
		label5.setBackground(lightBlue);

		panel1.setBackground(Color.GRAY);
		panel2.setBackground(Color.GRAY);
		panel3.setBackground(Color.GRAY);
		panel4.setBackground(Color.GRAY);
		panel5.setBackground(Color.GRAY);
		panel6.setBackground(Color.GRAY);
		upPanel.setBackground(Color.GRAY);
		downPanel.setBackground(Color.GRAY);

		panel1.add(label1);
		panel1.add(comboBox);

		panel2.add(label2);
		panel2.add(tfName);

		panel3.add(label3);
		panel3.add(tfPrice);

		panel4.add(label4);
		panel4.add(tfPhoto);

		panel5.add(label5);
		panel5.add(scrollPane);

		panel6.add(btnEdit);
		panel6.add(btnCreate);
		panel6.add(btnRemove);

		upPanel.add(panel1);
		upPanel.add(panel2);
		upPanel.add(panel3);
		upPanel.add(panel4);
		downPanel.add(panel5);
		// downPanel.add(panel6);

		mainPanel.add(upPanel);
		mainPanel.add(downPanel);

		mainMainPanel.add(mainPanel, BorderLayout.CENTER);
		mainMainPanel.add(panel6, BorderLayout.SOUTH);

		editFoodData.add(mainMainPanel);
		editFoodData.setVisible(true);

		comboBox.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {

				if (!comboBox.getSelectedItem().equals("добавить новое блюдо")) {
					btnEdit.setEnabled(true);
					btnRemove.setEnabled(true);
					btnCreate.setEnabled(false);

					try {
						tfName.setText(comboBox.getSelectedItem().toString());
						String price = map.get(comboBox.getSelectedItem()).getString(1);
						// setFilter(tfPrice, onlyNumbers, 20);
						tfPrice.setText(price);
						tfPhoto.setText(map.get(comboBox.getSelectedItem()).getString(2));
						taDescription.setText(map.get(comboBox.getSelectedItem()).getString(3));
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				} else {
					btnCreate.setEnabled(true);
					btnEdit.setEnabled(false);
					btnRemove.setEnabled(false);
					tfName.setText("");
					tfPrice.setText("");
					tfPhoto.setText("");
					taDescription.setText("");
				}

			}
		});

		btnEdit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (comboBox.getSelectedItem() != null) {
					try {
						Integer id = map.get(comboBox.getSelectedItem()).getInt(4);
						Database.editFoodTable(tableName, tfName.getText(), Integer.parseInt(tfPrice.getText()),
								tfPhoto.getText(), taDescription.getText(), id);
					} catch (SQLException e) {
						e.printStackTrace();
					} catch (NumberFormatException e) {
						ServerGUI.sendMsgToLog("Ошибка ! Неверные данные!");
					}

					editFoodData.dispose();
					windowApplyChange(tableName, sucsessChangeData);
					ServerGUI.sendMsgToLog(
							"Изменены данные в таблице " + tableName + " у блюда " + tfName.getText() + ".");
				}
			}
		});

		btnCreate.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Integer id = 1;
				String temp = tfName.getText();

				for (HashMap.Entry<String, ResultSet> pair : map.entrySet()) {
					id++;
				}

				if (temp.equals("") || tfPrice.getText().equals("") || tfPhoto.getText().equals("")
						|| taDescription.getText().equals("")) {
					windowApplyChange(tableName, errorChangeData);
					return;
				}

				try {
					Database.addNewToFoodTable(tableName, temp, Integer.parseInt(tfPrice.getText()), tfPhoto.getText(),
							taDescription.getText(), id);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				editFoodData.dispose();
				windowApplyChange(tableName, sucsessChangeData);
				ServerGUI.sendMsgToLog("Добавлено новое блюдо " + temp + " в таблицу " + tableName + ".");
			}
		});

		btnRemove.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String temp = tfName.getText();
				try {
					Database.removeFromFoodTable(tableName, temp);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				editFoodData.dispose();
				windowApplyChange(tableName, sucsessChangeData);
				ServerGUI.sendMsgToLog("Удалено блюдо " + temp + " из таблицы " + tableName + ".");
			}
		});
	}

	private void windowApplyChange(String tableName, String text) {
		windowApplyChange = new JFrame("");
		JPanel mainPanel = new JPanel(new FlowLayout());
		JLabel label = new JLabel(text);
		JButton btnOk = new JButton("OK");
		// text.setLineWrap(true);
		// text.setEditable(false);
		int width = 250;
		int height = 90;
		windowApplyChange.setSize(width, height);
		windowApplyChange.setResizable(false);
		windowApplyChange.setLocationRelativeTo(null);

		btnOk.setPreferredSize(new Dimension(160, 30));
		label.setOpaque(true);
		label.setBackground(lightBlue);
		mainPanel.setBackground(Color.GRAY);
		mainPanel.add(label);
		mainPanel.add(btnOk);

		windowApplyChange.add(mainPanel);
		windowApplyChange.setVisible(true);

		btnOk.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (tableName.equals("starting_text")) {
					editTableStartingText();
				} else {
					if (text.equals(sucsessChangeData)) {
						editFoodData(tableName);
					}
				}
				windowApplyChange.dispose();
			}
		});
	}

	private void sendChangeToLog(String tableName) {
		ServerGUI.sendMsgToLog("Изменены данные в таблице " + tableName);
	}

	private static void setFilter(JTextField field, String filter, int limit) {
		field.setDocument(new PlainDocument() {
			@Override
			public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
				if (filter.indexOf(str) != -1) {
					if (getLength() < limit) {
						super.insertString(offs, str, a);
					}
				}
			}
		});
	}
}
