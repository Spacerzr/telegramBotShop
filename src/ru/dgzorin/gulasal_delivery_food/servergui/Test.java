package ru.dgzorin.gulasal_delivery_food.servergui;

import java.awt.BorderLayout;
import java.awt.Container;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;

public class Test extends JFrame {
	JTextArea txt = new JTextArea(4, 20);

	Test() {
		Container cp = getContentPane();
		JTable table = new JTable(new DataModel());
		cp.add(new JScrollPane(table));
		cp.add(BorderLayout.SOUTH, txt);
		setSize(800, 600);
		setLocationRelativeTo(null);
	}

	class DataModel extends AbstractTableModel {
		Object[][] data = { { "one", "two", "three", "four" }, { "five", "six", "seven", "eight" },
				{ "nine", "ten", "eleven", "twelve" }, };

		// Печатает данные при изменении таблицы:
		class TML implements TableModelListener {
			public void tableChanged(TableModelEvent e) {
				txt.setText(""); // Очистка
				for (int i = 0; i < data.length; i++) {
					for (int j = 0; j < data[0].length; j++)
						txt.append(data[i][j] + " ");
					txt.append("\n");
				}
			}
		}

		public DataModel() {
			addTableModelListener(new TML());
		}

		public int getColumnCount() {
			return data[0].length;
		}

		public int getRowCount() {
			return data.length;
		}

		public Object getValueAt(int row, int col) {
			return data[row][col];
		}

		public void setValueAt(Object val, int row, int col) {
			data[row][col] = val;
			// Указывает на появление изменений:
			fireTableDataChanged();
		}

		public boolean isCellEditable(int row, int col) {
			return true;
		}
	}

	public static void start() {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new Test().setVisible(true);
			}
		});
	}
}