package ru.dgzorin.gulasal_delivery_food.servergui;

import ru.dgzorin.gulasal_delivery_food.database.Database;
import ru.dgzorin.gulasal_delivery_food.gdfbot.TelegramBot;

public class BotThread extends Thread {

	@Override
	public void run() {
		try {
			Database.connect();
			ServerGUI.sendMsgToLog("Connect to database was successful");
		} catch (Exception ex) {
			ServerGUI.sendMsgToLog("Connect to database : ERROR");
		}

		try {
			TelegramBot.startTelegramBot();
			ServerGUI.sendMsgToLog("Telegram Bot was started !\n");
		} catch (Exception ex) {
			ServerGUI.sendMsgToLog("Start Telegram Bot Fail !\n");
		}
	}

}
