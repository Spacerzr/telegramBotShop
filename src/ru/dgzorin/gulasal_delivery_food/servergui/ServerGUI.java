package ru.dgzorin.gulasal_delivery_food.servergui;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import ru.dgzorin.gulasal_delivery_food.database.Database;
import ru.dgzorin.gulasal_delivery_food.gdfbot.TelegramBot;

/*
 * Сервер с графическим интерфейсом 
 */

public class ServerGUI {

	private static final JTextArea log = new JTextArea(15, 30);

	private static SimpleDateFormat date = new SimpleDateFormat("dd.MM.yy 'at' HH.mm ':'");

	private JFrame viewServer;
	private JFrame viewspamSettings;
	private JFrame viewSpamForOneUseSettings;
	private BotThread test = new BotThread();
	private EditDataWindow viewEditData = new EditDataWindow();

	private Color lightGreen = new Color(180, 255, 180);
	private Color lightRed = new Color(255, 155, 155);
	private Color lightBlue = new Color(120, 175, 255);

	public static void main(String[] args) {
		SwingUtilities.invokeLater(ServerGUI::new);
	}

	ServerGUI() {
		initGUI();
	}

	private void initGUI() {

		viewServer = new JFrame();
		JPanel mainPanel = new JPanel(new FlowLayout());
		JPanel downPanel = new JPanel(new GridLayout(3, 2));
		JLabel telBotText = new JLabel("Telegram Bot");
		JLabel tmp = new JLabel("");

		int MAIN_WINDOW_WIDTH = 400;
		int MAIN_WINDOW_HEIGHT = 370;

		// JTextArea log = new JTextArea(15, 30);
		JScrollPane scrollPane = new JScrollPane(log, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		JButton btnStartTelBot = new JButton("Запустить");
		JButton btnStopTelBot = new JButton("Остановить");
		JButton btnSpamSettings = new JButton("Массовая рассылка");
		JButton btnSpamForOneUserSettings = new JButton("Сообщение 1 клиенту");

		JButton btnEditDB = new JButton("Редактировать");

		JLabel temp = new JLabel("");

		viewServer.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		viewServer.setSize(MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT);
		viewServer.setLocationRelativeTo(null);
		viewServer.setTitle("Gulasal delivery Food Server");
		viewServer.setResizable(false);
		log.setEditable(false);
		log.setLineWrap(true);
		log.setWrapStyleWord(true);

		// btnStopTelBot.setEnabled(false);
		btnStartTelBot.setEnabled(true);
		mainPanel.setBackground(Color.GRAY);
		telBotText.setOpaque(true);
		telBotText.setBackground(lightRed);
		tmp.setOpaque(true);
		tmp.setBackground(Color.GRAY);

		btnStartTelBot.setBackground(lightGreen);
		// btnSettings.setBackground(lightBlue);
		btnSpamSettings.setBackground(lightBlue);
		btnSpamForOneUserSettings.setBackground(lightBlue);

		btnEditDB.setBackground(lightBlue);
		temp.setOpaque(true);

		temp.setBackground(Color.GRAY);
		downPanel.add(telBotText);
		downPanel.add(btnStartTelBot);
		// downPanel.add(btnStopTelBot);
		// downPanel.add(btnSettings);
		downPanel.add(btnEditDB);
		downPanel.add(tmp);

		downPanel.add(btnSpamSettings);
		downPanel.add(btnSpamForOneUserSettings);

		mainPanel.add(scrollPane);
		mainPanel.add(downPanel);

		viewServer.add(mainPanel);

		viewServer.setVisible(true);

		btnStartTelBot.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				try {
					Database.connect();
					sendMsgToLog("Connect to database was successful");
				} catch (Exception ex) {
					sendMsgToLog("Connect to database : ERROR");
				}

				try {
					TelegramBot.startTelegramBot();
					sendMsgToLog("Telegram Bot was started !\n");
					btnStartTelBot.setEnabled(false);
					btnEditDB.setEnabled(false);
					// btnSpamSettings.setEnabled(false);
					// btnStopTelBot.setBackground(lightRed);
					// btnStopTelBot.setEnabled(true);
					telBotText.setBackground(lightGreen);
				} catch (Exception ex) {
					sendMsgToLog("Start Telegram Bot Fail !\n");
				}

			}
		});

		btnStopTelBot.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				// Тут нужен метод, который останавливает бота,
				// не закрывая приложение

				btnStartTelBot.setEnabled(true);
				btnStopTelBot.setEnabled(false);

			}
		});

		btnEditDB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				viewEditData.setVisible(true);
				sendMsgToLog("Редактирование данных ...");
			}
		});

		btnSpamSettings.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				initSpamSettingsWindow();
			}
		});

		btnSpamForOneUserSettings.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				initSpamForOneUseSettingsWindow();
			}
		});
	}

	private void initSpamSettingsWindow() {
		viewspamSettings = new JFrame();
		JPanel mainPanel = new JPanel(new GridLayout(3, 1));
		JPanel upPanel = new JPanel(new FlowLayout());
		JPanel centerPanel = new JPanel(new FlowLayout());
		JPanel downPanel = new JPanel(new FlowLayout());

		JLabel labelPhoto = new JLabel("Введите имя файла. Файл должен находиться в папке Photos.");
		JLabel labelText = new JLabel("Введите текст : ");
		JLabel labelInstr1 = new JLabel("Заполните поля и сохраните изменения.");
		JLabel labelInstr2 = new JLabel("Для отправки рассылки отправьте боту команду /sendSpam");

		JTextField namePhoto = new JTextField(20);
		JTextArea description = new JTextArea(4, 29);
		JScrollPane scrollPane = new JScrollPane(description, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		JButton btnSaveSettings = new JButton("Сохранить");

		int width = 400;
		int height = 300;

		viewspamSettings.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		viewspamSettings.setSize(width, height);
		viewspamSettings.setLocationRelativeTo(null);
		viewspamSettings.setTitle("Настройки массовой рассылки");
		viewspamSettings.setResizable(false);

		mainPanel.setBackground(Color.GRAY);
		upPanel.setBackground(Color.GRAY);
		centerPanel.setBackground(Color.GRAY);
		downPanel.setBackground(Color.GRAY);
		btnSaveSettings.setBackground(lightGreen);
		labelPhoto.setOpaque(true);
		labelPhoto.setBackground(lightBlue);
		labelText.setOpaque(true);
		labelText.setBackground(lightBlue);
		labelInstr1.setOpaque(true);
		labelInstr1.setBackground(lightBlue);
		labelInstr2.setOpaque(true);
		labelInstr2.setBackground(lightBlue);

		description.setLineWrap(true);
		description.setWrapStyleWord(true);
		namePhoto.setText(TelegramBot.spamPhotoName);
		description.setText(TelegramBot.spamDescription);

		upPanel.add(labelPhoto);
		upPanel.add(namePhoto);
		centerPanel.add(labelText);
		centerPanel.add(scrollPane);
		downPanel.add(labelInstr1);
		downPanel.add(labelInstr2);

		downPanel.add(btnSaveSettings);

		mainPanel.add(upPanel);
		mainPanel.add(centerPanel);
		mainPanel.add(downPanel);

		viewspamSettings.add(mainPanel);
		viewspamSettings.setVisible(true);

		btnSaveSettings.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				TelegramBot.setSpamSettings(namePhoto.getText(), description.getText());
				sendMsgToLog("Настройки массовой рассылки сохранены. Файл : " + namePhoto.getText() + ", Описание : "
						+ description.getText() + ".\n");
				viewspamSettings.dispose();
			}
		});
	}

	private void initSpamForOneUseSettingsWindow() {
		viewSpamForOneUseSettings = new JFrame();
		JPanel mainPanel = new JPanel(new GridLayout(3, 1));
		JPanel upPanel = new JPanel(new FlowLayout());
		JPanel centerPanel = new JPanel(new FlowLayout());
		JPanel downPanel = new JPanel(new FlowLayout());

		JLabel labelChatId = new JLabel(" Введите Chat ID клиента, которому будет отправлено сообщение.");
		JLabel labelText = new JLabel(" Введите текст : ");
		JLabel labelInstr1 = new JLabel(" Заполните поля и сохраните изменения.");
		JLabel labelInstr2 = new JLabel(" Для отправки сообщения одному клиенту ");
		JLabel labelInstr3 = new JLabel("    отправьте боту команду /sendMsgForOneUser   \n ");

		JTextField nameChatId = new JTextField(20);
		JTextArea description = new JTextArea(4, 29);
		JScrollPane scrollPane = new JScrollPane(description, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		JButton btnSaveSettings = new JButton("Сохранить");

		int width = 400;
		int height = 330;

		viewSpamForOneUseSettings.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		viewSpamForOneUseSettings.setSize(width, height);
		viewSpamForOneUseSettings.setLocationRelativeTo(null);
		viewSpamForOneUseSettings.setTitle("Настройки сообщения одному клиенту");
		viewSpamForOneUseSettings.setResizable(false);

		mainPanel.setBackground(Color.GRAY);
		upPanel.setBackground(Color.GRAY);
		centerPanel.setBackground(Color.GRAY);
		downPanel.setBackground(Color.GRAY);
		btnSaveSettings.setBackground(lightGreen);
		labelChatId.setOpaque(true);
		labelChatId.setBackground(lightBlue);
		labelText.setOpaque(true);
		labelText.setBackground(lightBlue);
		labelInstr1.setOpaque(true);
		labelInstr1.setBackground(lightBlue);
		labelInstr2.setOpaque(true);
		labelInstr2.setBackground(lightBlue);
		labelInstr3.setOpaque(true);
		labelInstr3.setBackground(lightBlue);

		description.setLineWrap(true);
		description.setWrapStyleWord(true);
		nameChatId.setText(TelegramBot.chatIdForMsgToOneUser);
		description.setText(TelegramBot.spamForOneUser);

		upPanel.add(labelChatId);
		upPanel.add(nameChatId);
		centerPanel.add(labelText);
		centerPanel.add(scrollPane);
		downPanel.add(labelInstr1);
		downPanel.add(labelInstr2);
		downPanel.add(labelInstr3);

		downPanel.add(btnSaveSettings);

		mainPanel.add(upPanel);
		mainPanel.add(centerPanel);
		mainPanel.add(downPanel);

		viewSpamForOneUseSettings.add(mainPanel);
		viewSpamForOneUseSettings.setVisible(true);

		btnSaveSettings.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				TelegramBot.setSpamForOneUserSettings(nameChatId.getText(), description.getText());
				sendMsgToLog("Настройки отправки сообщения одному клиенту сохранены. ChatId : " + nameChatId.getText()
						+ ", Сообщение : " + description.getText() + ".\n");
				viewSpamForOneUseSettings.dispose();
			}
		});
	}

	//
	public static void sendMsgToLog(String msg) {
		log.append(date.format(new Date()) + " " + msg + "\n");
	}
}
