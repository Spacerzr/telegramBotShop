package ru.dgzorin.gulasal_delivery_food.gdfbot;

import java.util.ArrayList;

import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

/*
 * класс для инициализации клавиатур под чатом бота
 */
public class Keyboards {

	private KeyboardButton kbMenu = new KeyboardButton("🍽 Меню");
	private KeyboardButton kbDeliveryPay = new KeyboardButton("🚚 Доставка и оплата 💰");
	private KeyboardButton kbCart = new KeyboardButton("🛒 Корзина");
	private KeyboardButton kbCallManager = new KeyboardButton("☎️ Связь с менеджером");

	private KeyboardButton kbHome = new KeyboardButton("🏡 Главное меню");
	private KeyboardButton kbClearCart = new KeyboardButton("❌ Очистить корзину");
	private KeyboardButton kbReturnToCart = new KeyboardButton("🛒 Вернуться в корзину");
	private KeyboardButton kbSendOrder = new KeyboardButton("✅ Отправить заказ ! ✅");
	private KeyboardButton kbRegOrder = new KeyboardButton("✅ Оформить заказ");
	private KeyboardButton kbContinueShoipping = new KeyboardButton("🍽 Продолжить покупки");
	private KeyboardButton kbChangeClientData = new KeyboardButton("🔃 Изменить");

	private KeyboardButton kbSoups = new KeyboardButton("🍜 Супы");
	private KeyboardButton kbSalats = new KeyboardButton("🥗 Салаты");
	private KeyboardButton kbHot = new KeyboardButton("🍖 Горячие блюда");
	private KeyboardButton kbPastry = new KeyboardButton("🥐 Выпечка");

	private KeyboardButton kbPayToCourier = new KeyboardButton("🚛 Наличными курьеру");
	private KeyboardButton kbPayInTelegram = new KeyboardButton("Оплата в Telegram 💳");

	private KeyboardButton kbNextToPhone = new KeyboardButton("Продолжить ➡️ ☎️");
	private KeyboardButton kbNextToComment = new KeyboardButton("Продолжить ➡️ 📝");
	private KeyboardButton kbNextToPay = new KeyboardButton("Продолжить ➡️ 💰");

	ArrayList<KeyboardRow> startMenuKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbMenu);
		keyboardFirstRow.add(kbDeliveryPay);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbCart);
		keyboardSecondRow.add(kbCallManager);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> foodMenuKeyboard() {

		// Создаем список строк клавиатуры
		ArrayList<KeyboardRow> keyboard = new ArrayList<>();

		// Первая строчка клавиатуры
		KeyboardRow keyboardFirstRow = new KeyboardRow();
		// Добавляем кнопки в первую строчку клавиатуры
		keyboardFirstRow.add(kbSoups);
		keyboardFirstRow.add(kbSalats);

		// Вторая строчка клавиатуры
		KeyboardRow keyboardSecondRow = new KeyboardRow();
		// Добавляем кнопки во вторую строчку клавиатуры
		keyboardSecondRow.add(kbHot);
		keyboardSecondRow.add(kbPastry);
		KeyboardRow keyboardThirdRow = new KeyboardRow();
		// Добавляем кнопки во вторую строчку клавиатуры
		keyboardThirdRow.add(kbHome);
		keyboardThirdRow.add(kbCart);

		// Добавляем все строчки клавиатуры в список
		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);
		keyboard.add(keyboardThirdRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> cartMenuKeyboard() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbContinueShoipping);
		keyboardFirstRow.add(kbRegOrder);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbHome);
		keyboardSecondRow.add(kbClearCart);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> cartMenuChangeData() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbChangeClientData);
		keyboardFirstRow.add(kbNextToComment);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbHome);
		keyboardSecondRow.add(kbReturnToCart);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> cartMenuStepAddress() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbNextToPhone);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbHome);
		keyboardSecondRow.add(kbReturnToCart);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> cartMenuStepPhoneNumber() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbNextToComment);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbHome);
		keyboardSecondRow.add(kbReturnToCart);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> cartMenuStepComment() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbNextToPay);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbHome);
		keyboardSecondRow.add(kbReturnToCart);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> cartMenuStepChoosePay() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();

		keyboardFirstRow.add(kbPayToCourier);
		keyboardFirstRow.add(kbPayInTelegram);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbHome);
		keyboardSecondRow.add(kbReturnToCart);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;
	}

	ArrayList<KeyboardRow> cartMenuSendOrder1() {

		ArrayList<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();
		keyboardFirstRow.add(kbSendOrder);

		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(kbHome);
		keyboardSecondRow.add(kbReturnToCart);

		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);

		return keyboard;
	}

}
