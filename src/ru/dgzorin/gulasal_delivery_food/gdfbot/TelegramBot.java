package ru.dgzorin.gulasal_delivery_food.gdfbot;

import java.io.File;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.api.methods.AnswerPreCheckoutQuery;
import org.telegram.telegrambots.api.methods.send.SendInvoice;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.payments.LabeledPrice;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.exceptions.TelegramApiValidationException;

import ru.dgzorin.gulasal_delivery_food.database.Database;
import ru.dgzorin.gulasal_delivery_food.servergui.ServerGUI;;

/*
 * Класс для настройки телеграм-бота
 */
public class TelegramBot extends TelegramLongPollingBot {

	private SendMessage sendMessage = new SendMessage();
	private Keyboards chooseKeyboard = new Keyboards();
	private ArrayList<KeyboardRow> keyboard = new ArrayList<>();

	private static HashMap<String, HashMap<String, Integer>> carts = new HashMap<String, HashMap<String, Integer>>();
	private static HashMap<String, String> orders = new HashMap<String, String>();

	private static HashMap<String, String> clients = new HashMap<String, String>();

	private static HashMap<String, String> addresses = new HashMap<String, String>();
	private static HashMap<String, Boolean> addressFlags = new HashMap<String, Boolean>();

	private static HashMap<String, String> phoneNumbers = new HashMap<String, String>();
	private static HashMap<String, Boolean> PhoneNumbersFlags = new HashMap<String, Boolean>();

	private static HashMap<String, String> comments = new HashMap<String, String>();
	private static HashMap<String, Boolean> commentsFlags = new HashMap<String, Boolean>();

	private static HashMap<String, Integer> totalPrices = new HashMap<String, Integer>();
	private static int minPrice = 500;

	private static String startMessage = "";
	private static String menuMessage = "";
	private static String deliveryMessage = "";
	private static String callmanagerMessage = "";
	private static String soupsMessage = "";
	private static String salatsMessage = "";
	private static String pastryMessage = "";
	private static String HotMessage = "";
	private static String developerMessage = "";
	private static String wrongMessage = "";
	private static String emptyCart = "";
	private static String regOrder = "";

	private static String emailLogin = "shoptelegrambot@gmail.com"; // ящик откуда улетают заказы
	private static String emailPassword = "fNMb#UzIw*"; // пароль от исходящего ящика
	private static String emailForReceivingOrders = "ordersfromourshop@gmail.com"; // ящик куда прилетают заказы

	private static MailSender emailSender = new MailSender(emailLogin, emailPassword);
	SimpleDateFormat date = new SimpleDateFormat("dd.MM.yy 'at' HH.mm ':'");

	private static String adminChatId = "308710480";
	public static String spamPhotoName = "spam.jpg";
	public static String spamDescription = "Чайхона \"Gul-Asal\" желает Вам доброго утра и отличного настроения !";

	public static String spamForOneUser = "Уважаемый клиент ! Указаны неправильные контактные данные, "
			+ "из-за чего мы не можем связаться с Вами. Пожалуйста, укажите корректные адрес и телефон и повторите заказ.";
	public static String chatIdForMsgToOneUser = "435403963";

	public static void startTelegramBot() {

		initMessageText();

		ApiContextInitializer.init();
		TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
		try {
			telegramBotsApi.registerBot(new TelegramBot());
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}

	}

	// public static void stopTelegramBot() {
	//
	// }

	@Override
	public String getBotUsername() {
		return "Gulasal_delivery_food_bot";
	}

	@Override
	public String getBotToken() {
		return "548185634:AAHpKCvSxggKnPSzGkXPDr_YUQmVB7gwwh8";
	}

	@Override
	public void onUpdateReceived(Update update) {

		Message message = update.getMessage();

		Boolean addressFlag = false;
		Boolean phoneNumberFlag = false;
		Boolean commentFlag = false;

		if (message != null && message.hasText()) {
			String chat_id = message.getChatId().toString();
			// String clientNick = message.getFrom().getUserName();
			String clientName = message.getFrom().getFirstName();
			if (message.getFrom().getLastName() != null) {
				clientName += " " + message.getFrom().getLastName();
			}

			if (!clients.containsKey(chat_id)) {
				clients.put(chat_id, clientName);
			}

			if (addressFlags.containsKey(chat_id)) {
				addressFlag = addressFlags.get(chat_id);
			}

			if (PhoneNumbersFlags.containsKey(chat_id)) {
				phoneNumberFlag = PhoneNumbersFlags.get(chat_id);
			}

			if (commentsFlags.containsKey(chat_id)) {
				commentFlag = commentsFlags.get(chat_id);
			}

			switch (message.getText()) {

			case "/sendSpam":
				sendSpam(message.getChatId().toString());
				break;
			case "/sendMsgForOneUser":
				sendSpamForOneUser(message.getChatId().toString());
				break;

			// главное меню
			case "/start":
			case "🏡 Главное меню":
				addressFlags.remove(chat_id);
				PhoneNumbersFlags.remove(chat_id);
				commentsFlags.remove(chat_id);
				keyboard = chooseKeyboard.startMenuKeyboard();
				sendMsg(message, keyboard, startMessage);
				break;

			case "🍽 Меню":
			case "🍽 Продолжить покупки":
				keyboard = chooseKeyboard.foodMenuKeyboard();
				sendMsg(message, keyboard, menuMessage);
				break;

			case "🚚 Доставка и оплата 💰":
				keyboard = chooseKeyboard.startMenuKeyboard();
				sendMsg(message, keyboard, deliveryMessage);
				break;

			case "🛒 Корзина":
			case "🛒 Вернуться в корзину":
				keyboard = chooseKeyboard.cartMenuKeyboard();
				showCart(message, keyboard, chat_id);
				addressFlags.remove(chat_id);
				PhoneNumbersFlags.remove(chat_id);
				commentsFlags.remove(chat_id);
				break;

			case "☎️ Связь с менеджером":
				keyboard = chooseKeyboard.startMenuKeyboard();
				sendMsg(message, keyboard, callmanagerMessage);
				break;

			// Меню корзины
			case "✅ Оформить заказ":
				if (getCart(message, chat_id).equals("Ваша корзина пуста\n")) {
					keyboard = chooseKeyboard.foodMenuKeyboard();
					sendMsg(message, keyboard, emptyCart);
				} else {
					int price = totalPrices.get(chat_id);
					if (price < minPrice) {
						keyboard = chooseKeyboard.foodMenuKeyboard();
						sendMsg(message, keyboard,
								"Извините, но минимальная сумма для заказа 500 рублей. Пожалуйста, добавьте что-нибудь еще из нашего меню.");
						break;
					} else {
						String msgToStep1 = "";

						String address = Database.getAddress(chat_id);
						String phoneNumber = Database.getPhoneNumber(chat_id);

						if (!address.equals("") && !address.equals(null) && !phoneNumber.equals("")
								&& !phoneNumber.equals(null)) {
							msgToStep1 = "Вы уже вводили ранее данные: \n" + "адрес для доставки - \"" + address
									+ "\";\n" + "телефон - \"" + phoneNumber + "\".\n"
									+ "Если данные актуальны, нажмите \"Продолжить ➡️\",\nлибо \"🔃 Изменить\" для их редактирования.";

							keyboard = chooseKeyboard.cartMenuChangeData();
							sendMsg(message, keyboard, msgToStep1);
							break;
						} else {
							msgToStep1 = "Отправьте нам адрес доставки (улица, дом, квартира)";
							keyboard = chooseKeyboard.cartMenuStepAddress();
							addressFlags.put(chat_id, true);
							sendMsg(message, keyboard, msgToStep1);
							break;
						}
					}
				}
				break;

			case "🔃 Изменить":
				addressFlags.put(chat_id, true);
				keyboard = chooseKeyboard.cartMenuStepAddress();
				String msg = "Отправьте нам адрес доставки (улица, дом, квартира)";

				String address = Database.getAddress(chat_id);
				if (!address.equals("") && !address.equals(null)) {
					msg = "Вы уже вводили ранее адрес для доставки - \"" + address
							+ "\". Если он актуален, нажмите \"Продолжить ➡️\", либо отправьте нам новый адрес";
					addresses.put(chat_id, address);
				}
				sendMsg(message, keyboard, msg);
				break;

			case "❌ Очистить корзину":
				keyboard = chooseKeyboard.startMenuKeyboard();
				clearCart(chat_id);
				sendMsg(message, keyboard, "Корзина очищена !");
				break;

			case "Продолжить ➡️ ☎️": // кнопка перехода из адреса в телефон
				if (Database.getAddress(chat_id).equals("")) {
					String errorAddressMsg = "Мы не знаем куда везти заказ. Пожалуйста, отправьте нам адрес.";
					keyboard = chooseKeyboard.cartMenuStepAddress();
					sendMsg(message, keyboard, errorAddressMsg);
					break;
				}

				addressFlags.remove(chat_id);
				PhoneNumbersFlags.put(chat_id, true);

				keyboard = chooseKeyboard.cartMenuStepPhoneNumber();
				String msgToStep1 = "Отправьте нам номер телефона для связи с Вами.";

				String phoneNumber = Database.getPhoneNumber(chat_id);
				if (!phoneNumber.equals("") && !phoneNumber.equals(null)) {
					msgToStep1 = "Вы уже вводили ранее номер телефона - \"" + phoneNumber
							+ "\". Если он актуален, нажмите \"Продолжить ➡️\", либо отправьте нам новый номер";
					phoneNumbers.put(chat_id, phoneNumber);
				}
				sendMsg(message, keyboard, msgToStep1);
				break;

			case "Продолжить ➡️ 💰": // нопка перехода из комментария в оплату
				keyboard = chooseKeyboard.cartMenuStepChoosePay();
				commentsFlags.remove(chat_id);
				regOrderStep2(message, keyboard, chat_id);

				break;

			case "Продолжить ➡️ 📝": // ккнопка перехода из телефона в комментарий
				if (Database.getPhoneNumber(chat_id).equals("")) {
					String errorAddressMsg = "Мы не знаем как связаться с Вами. Пожалуйста, отправьте нам свой номер телефона.";
					keyboard = chooseKeyboard.cartMenuStepPhoneNumber();
					sendMsg(message, keyboard, errorAddressMsg);
					break;
				}
				String msgToStep2 = "Отправьте нам Ваш комментарий к заказу, например желаемое время доставки и нажмите \"Продолжить ➡️\".";
				keyboard = chooseKeyboard.cartMenuStepComment();
				PhoneNumbersFlags.remove(chat_id);
				commentsFlags.put(chat_id, true);
				sendMsg(message, keyboard, msgToStep2);
				break;

			case "🚛 Наличными курьеру":
				keyboard = chooseKeyboard.cartMenuSendOrder1();
				String order = regOrderStep3(message, keyboard, chat_id);
				orders.put(chat_id, order);
				break;

			case "Оплата в Telegram 💳":
				keyboard = chooseKeyboard.cartMenuStepChoosePay();
				// sendMsg(message, keyboard, "Сервис временно недоступен. Воспользуйтесь
				// оплатой курьеру.");
				internalPayment(message, chat_id);
				break;

			case "✅ Отправить заказ ! ✅":
				keyboard = chooseKeyboard.startMenuKeyboard();

				// String finishOrder = orders.get(chat_id);
				// emailSender.send("" + date.format(new Date()) + " Новый заказ из Telegram !",
				// "ChatId Клиента: " + chat_id + "\n" + finishOrder, emailForReceivingOrders);
				// ServerGUI.sendMsgToLog("Новый заказ !");
				sendFinishOrder(chat_id);
				sendMsg(message, keyboard, regOrder);
				break;

			// Меню "блюда"
			case "🍜 Супы":
				String tableNameSoups = "soups";
				sendMsg(message, soupsMessage);
				// sendMsgFoodList(message, Database.gedFoodList("soups"));
				sendMsgFoodList(message, tableNameSoups);
				break;

			case "🥗 Салаты":
				String tableNameSalats = "salats";
				sendMsg(message, salatsMessage);
				sendMsgFoodList(message, tableNameSalats);
				break;

			case "🥐 Выпечка":
				String tableNamePastry = "pastry";
				sendMsg(message, pastryMessage);
				sendMsgFoodList(message, tableNamePastry);
				break;

			case "🍖 Горячие блюда":
				String tableNameHot = "hot";

				sendMsg(message, HotMessage);
				sendMsgFoodList(message, tableNameHot);
				break;

			case "/developer":
				keyboard = chooseKeyboard.startMenuKeyboard();
				sendMsg(message, keyboard, developerMessage);
				break;

			default:
				if (!addressFlag && !phoneNumberFlag && !commentFlag) {
					sendMsg(message, keyboard, wrongMessage);
					break;
				}
			}
		} else if (update.hasCallbackQuery() && !addressFlag && !phoneNumberFlag) {

			String call_data = update.getCallbackQuery().getData();
			HashMap<String, Integer> map = Database.getAllMenu();
			String message_text = "";

			String chat_id = update.getCallbackQuery().getMessage().getChatId().toString();

			for (HashMap.Entry<String, Integer> pair : map.entrySet()) {
				if (call_data.equals(pair.getKey())) {
					addToCart(chat_id, pair.getKey());
					message_text = "Блюдо " + pair.getKey() + " Добавлено в корзину !";
				}
			}
			SendMessage message1 = new SendMessage().setChatId(chat_id).setText(message_text);
			try {
				execute(message1);
			} catch (TelegramApiException e) {
				e.printStackTrace();
			}
		}
		// Метод для ввода и обработки адреса или телефона от пользователя
		if (update.hasMessage() && addressFlag) {
			regOrderStepAddress(message, keyboard, message.getChatId().toString(), update.getMessage().getText());
		}

		if (update.hasMessage() && phoneNumberFlag) {
			regOrderStepPhoneNumber(message, keyboard, message.getChatId().toString(), update.getMessage().getText());
		}

		if (update.hasMessage() && commentFlag) {
			regOrderStepComment(message, keyboard, message.getChatId().toString(), update.getMessage().getText());
		}
		if (update.hasPreCheckoutQuery()) {
			InternalPay(update.getPreCheckoutQuery().getId());
			keyboard = chooseKeyboard.startMenuKeyboard();
			String chat_id = update.getPreCheckoutQuery().getFrom().getId().toString();
			sendFinishOrder(chat_id);
			sendMsg(chat_id, keyboard, regOrder);
		}

	}

	private void InternalPay(String id) {
		AnswerPreCheckoutQuery answer = new AnswerPreCheckoutQuery();
		answer.getPreCheckoutQueryId();
		answer.setOk(true);
		answer.setPreCheckoutQueryId(id);
		answer.setErrorMessage("Ошибка проведения транзакции !");

		try {
			answer.validate();
			execute(answer);
		} catch (TelegramApiValidationException e1) {
			e1.printStackTrace();
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
	}

	private void sendFinishOrder(String chat_id) {

		String finishOrder = orders.get(chat_id);
		emailSender.send("" + date.format(new Date()) + " Новый заказ из Telegram !",
				"ChatId Клиента: " + chat_id + "\nИмя Клиента: " + clients.get(chat_id) + "\n" + finishOrder,
				emailForReceivingOrders);
		ServerGUI.sendMsgToLog("Новый заказ !");
		carts.remove(chat_id);
	}

	// Отправка сообщений
	private void sendMsg(Message message, String text) {

		sendMessage.setChatId(message.getChatId().toString());
		sendMessage.setReplyToMessageId(message.getMessageId());
		sendMessage.setText(text);
		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
	}

	private void sendMsg(Message message, ArrayList<KeyboardRow> keyboard, String text) {

		sendMessage.enableMarkdown(true);

		// Создаем клавиуатуру
		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		replyKeyboardMarkup.setKeyboard(keyboard);

		sendMessage.setChatId(message.getChatId().toString());
		sendMessage.setReplyToMessageId(message.getMessageId());
		sendMessage.setText(text);
		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
	}

	private void sendMsg(String chat_id, ArrayList<KeyboardRow> keyboard, String text) {

		sendMessage.enableMarkdown(true);
		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		replyKeyboardMarkup.setKeyboard(keyboard);
		sendMessage.setChatId(chat_id);
		sendMessage.setText(text);
		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
	}

	// Отправка меню
	private void sendMsgFoodList(Message message, String tableName) {

		try {
			ResultSet rs = Database.getFoodList(tableName);

			while (rs.next()) {
				SendPhoto sendPhotoRequest = new SendPhoto();
				File file;
				String photoName = rs.getString(3);

				InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
				List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
				List<InlineKeyboardButton> rowInline = new ArrayList<>();
				if (photoName == null || photoName.equals("")) {
					file = new File("photos/missing_foto.jpg");
				} else {
					file = new File("photos/" + photoName);
				}

				rowInline.add(new InlineKeyboardButton().setText(rs.getInt(2) + " рублей   ❇️")
						.setCallbackData(rs.getString(1)));
				rowsInline.add(rowInline);
				markupInline.setKeyboard(rowsInline);

				sendPhotoRequest.setChatId(message.getChatId().toString());

				sendPhotoRequest.setNewPhoto(file);
				sendPhotoRequest.setCaption(rs.getString(1) + "\n" + rs.getString(4));
				sendPhotoRequest.setReplyMarkup(markupInline);

				try {
					sendPhoto(sendPhotoRequest);
				} catch (TelegramApiException ex) {
					file = new File("photos/missing_foto.jpg");
					sendPhotoRequest.setNewPhoto(file);
					sendPhoto(sendPhotoRequest);
					ServerGUI.sendMsgToLog("Ошибка имени файла фотографии.\nПроверьте БД.");
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendSpam(String chat_id) {

		if (!chat_id.equals(adminChatId)) {
			return;
		}

		ArrayList<String> list = new ArrayList<>();
		list = Database.getChatIdList();
		SendPhoto sendPhotoRequest = new SendPhoto();
		File file = new File("photos/" + spamPhotoName);

		sendPhotoRequest.setNewPhoto(file);
		sendPhotoRequest.setCaption(spamDescription);

		for (String id : list) {

			sendPhotoRequest.setChatId(id);
			try {
				sendPhoto(sendPhotoRequest);
			} catch (TelegramApiException ex) {
				ServerGUI.sendMsgToLog("Ошибка имени файла фотографии.\nПроверьте БД.");
			}
		}
	}

	private void sendSpamForOneUser(String chat_id) {
		if (!chat_id.equals(adminChatId)) {
			return;
		}
		SendPhoto sendPhotoRequest = new SendPhoto();
		File file = new File("photos/smile_bad_order.jpg");

		sendPhotoRequest.setNewPhoto(file);
		sendPhotoRequest.setCaption(spamForOneUser);
		sendPhotoRequest.setChatId(chatIdForMsgToOneUser);
		try {
			sendPhoto(sendPhotoRequest);
		} catch (TelegramApiException ex) {
			ServerGUI.sendMsgToLog("Ошибка имени файла фотографии.\nПроверьте БД.");
		}

	}

	// Методы для работы с корзиной

	private void searchInCarts(String chat_id) {
		if (carts.containsKey(chat_id)) {
			return;
		}
		carts.put(chat_id, new HashMap<String, Integer>());
		searchInDbClients(chat_id);
	}

	private void searchInDbClients(String chat_id) {

		if (Database.getClinetChatId(chat_id)) {
			return;
		}
		Database.addNewClient(chat_id, clients.get(chat_id));
	}

	private void addToCart(String chat_id, String name) {

		searchInCarts(chat_id);

		for (HashMap.Entry<String, HashMap<String, Integer>> pair : carts.entrySet()) {
			if (pair.getKey().equals(chat_id)) {
				boolean tmp = true;
				for (HashMap.Entry<String, Integer> pair2 : pair.getValue().entrySet()) {
					if (pair2.getKey().equals(name)) {
						pair2.setValue(pair2.getValue() + 1);
						tmp = false;
						break;
					}
				}
				if (tmp) {
					pair.getValue().put(name, 1);
				}
			}
		}
	}

	private String getCart(Message message, String chat_id) {

		sendMessage.setChatId(chat_id);
		sendMessage.setReplyToMessageId(message.getMessageId());
		String text = "Ваша корзина пуста\n";
		HashMap<String, Integer> temp = Database.getAllMenu();
		int price = 0;
		int totalPrice = 0;

		HashMap<String, Integer> cart = carts.get(chat_id);
		if (cart == null) {
			return text;
		}

		text = "Ваш заказ :\n";
		for (HashMap.Entry<String, Integer> pair2 : cart.entrySet()) {
			for (HashMap.Entry<String, Integer> pair3 : temp.entrySet()) {
				if (pair3.getKey().equals(pair2.getKey())) {
					price = pair3.getValue();
				}
			}
			text += pair2.getKey() + " : " + pair2.getValue() + " шт., " + price * pair2.getValue() + "руб.\n";
			totalPrice += price * pair2.getValue();
		}
		text += "Итоговая сумма : " + totalPrice + " рублей.\n";
		totalPrices.put(chat_id, totalPrice);
		return text;
	}

	private void showCart(Message message, ArrayList<KeyboardRow> keyboard, String chat_id) {

		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		replyKeyboardMarkup.setKeyboard(keyboard);

		String text = getCart(message, chat_id);

		sendMessage.setChatId(chat_id);
		sendMessage.setReplyToMessageId(message.getMessageId());

		sendMessage.setText(text);
		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
	}

	private void clearCart(String chat_id) {
		carts.remove(chat_id);
		totalPrices.remove(chat_id);
	}

	// Оформление заказа

	private void regOrderStepAddress(Message message, ArrayList<KeyboardRow> keyboard, String chat_id, String address) {

		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		replyKeyboardMarkup.setKeyboard(keyboard);

		sendMessage.setChatId(chat_id);
		sendMessage.setReplyToMessageId(message.getMessageId());

		String nextStep = "Нажмите кнопку \"Продолжить ➡️ ☎️\" если адрес верный, либо отправьте нам другой адрес.";
		if (!address.equals("Продолжить ➡️ ☎️") && !address.equals("🏡 Главное меню")
				&& !address.equals("🛒 Вернуться в корзину")) {
			addresses.put(chat_id, address);
			Database.addClientAddress(chat_id, address);
		} else {
			return;
		}

		sendMessage.setText(nextStep);
		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
	}

	private void regOrderStepPhoneNumber(Message message, ArrayList<KeyboardRow> keyboard, String chat_id,
			String phoneNumber) {

		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		replyKeyboardMarkup.setKeyboard(keyboard);

		sendMessage.setChatId(chat_id);
		sendMessage.setReplyToMessageId(message.getMessageId());

		String nextStep = "Нажмите кнопку \"Продолжить ➡️ 📝\" если номер телефона верный, либо отправьте нам другой номер.";
		if (!phoneNumber.equals("Продолжить ➡️ 📝") && !phoneNumber.equals("🏡 Главное меню")
				&& !phoneNumber.equals("🛒 Вернуться в корзину")) {
			phoneNumbers.put(chat_id, phoneNumber);
			Database.addClientPhoneNumber(chat_id, phoneNumber);
		} else {
			return;
		}

		sendMessage.setText(nextStep);
		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
	}

	private void regOrderStepComment(Message message, ArrayList<KeyboardRow> keyboard, String chat_id, String comment) {

		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		replyKeyboardMarkup.setKeyboard(keyboard);

		sendMessage.setChatId(chat_id);
		sendMessage.setReplyToMessageId(message.getMessageId());

		String nextStep = "Нажмите кнопку \"Продолжить ➡️ 💰\" если комментарий верный, либо отправьте нам другой комментарий.";
		if (!comment.equals("Продолжить ➡️ 💰") && !comment.equals("🏡 Главное меню")
				&& !comment.equals("🛒 Вернуться в корзину")) {
			comments.put(chat_id, comment);
		} else {
			return;
		}

		sendMessage.setText(nextStep);
		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
	}

	private void regOrderStep2(Message message, ArrayList<KeyboardRow> keyboard, String chat_id) {

		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		replyKeyboardMarkup.setKeyboard(keyboard);

		sendMessage.setChatId(chat_id);
		sendMessage.setReplyToMessageId(message.getMessageId());

		String nextStep = "💰 Выберите способ оплаты :";

		sendMessage.setText(nextStep);
		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
	}

	private String regOrderStep3(Message message, ArrayList<KeyboardRow> keyboard, String chat_id) {

		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(false);
		replyKeyboardMarkup.setKeyboard(keyboard);

		sendMessage.setChatId(chat_id);
		sendMessage.setReplyToMessageId(message.getMessageId());

		String order = getOrder(message, chat_id);
		String paymentToCourier = "Оплата курьеру.\n";
		String comment = "Комментарий к заказу : " + comments.get(chat_id) + "\n";
		order += comment += paymentToCourier + "\n";

		sendMessage.setText(order);
		try {
			execute(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
		return order;
	}

	private void internalPayment(Message message, String chat_id) {
		SendInvoice invoice = new SendInvoice();
		ArrayList<LabeledPrice> prices = new ArrayList<LabeledPrice>();
		LabeledPrice price = new LabeledPrice();

		int totalPrice = totalPrices.get(chat_id) * 100;

		String order = getOrder(message, chat_id);
		String paymentFromBot = "Оплата через Telegram.\n";
		String comment = "Комментарий к заказу : " + comments.get(chat_id) + "\n";
		order += comment += paymentFromBot + "\n";

		price.setLabel("Сумма к оплате: ");
		price.setAmount(totalPrice);

		prices.add(price);

		invoice.setChatId(Integer.parseInt(chat_id));
		invoice.setTitle("Проверьте заказ");
		invoice.setDescription(order);
		invoice.setPayload(chat_id);
		invoice.setProviderToken("381764678:TEST:4759");
		invoice.setStartParameter(chat_id);
		invoice.setCurrency("RUB");
		invoice.setPrices(prices);
		orders.put(chat_id, order);

		try {
			execute(invoice);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}

	}

	private String getOrder(Message message, String chat_id) {

		String order = "";
		String cart = getCart(message, chat_id);
		String address = "Доставить по адресу: \n";
		String phoneNumber = "Телефон для связи : \n";
		// if (addresses.containsKey(chat_id)) {
		// for (HashMap.Entry<String, String> pair : addresses.entrySet()) {
		// if (pair.getKey().equals(chat_id)) {
		// address += pair.getValue() + "\n";
		// }
		// }
		// } else {
		// address += "Адрес не указан.\n";
		// }

		// if (phoneNumbers.containsKey(chat_id)) {
		// for (HashMap.Entry<String, String> pair : phoneNumbers.entrySet()) {
		// if (pair.getKey().equals(chat_id)) {
		// phoneNumber += pair.getValue() + "\n";
		// }
		// }
		// } else {
		// phoneNumber += "Номер телефона не указан.\n";
		// }
		address += Database.getAddress(chat_id) + "\n";
		phoneNumber += Database.getPhoneNumber(chat_id) + "\n";
		order += cart += address += phoneNumber;

		return order;
	}

	// Загрузка сообщений бота из базы
	private static void initMessageText() {

		HashMap<String, String> startingText = Database.getStartingText();
		startMessage = startingText.get("/start");
		menuMessage = startingText.get("/menu");
		deliveryMessage = startingText.get("/delivery");
		callmanagerMessage = startingText.get("/callmanager");
		soupsMessage = startingText.get("/soups");
		salatsMessage = startingText.get("/salats");
		pastryMessage = startingText.get("/pastry");
		HotMessage = startingText.get("/hot");
		developerMessage = startingText.get("/developer");
		wrongMessage = startingText.get("/wrong");
		emptyCart = startingText.get("/emptycart");
		regOrder = startingText.get("/regorder");
	}

	public static void setSpamSettings(String spamPhoto, String description) {
		spamPhotoName = spamPhoto;
		spamDescription = description;
	}

	public static void setSpamForOneUserSettings(String chatId, String description) {
		chatIdForMsgToOneUser = chatId;
		spamForOneUser = description;
	}
}
